
import { ButtonBase, Grid, List, ListItem, ListItemIcon, ListItemText, styled, Typography } from '@mui/material'
import React from 'react'
import CollectionsIcon from '@mui/icons-material/Collections';
import GroupIcon from '@mui/icons-material/Group';
import StarIcon from '@mui/icons-material/Star';
import AccessTimeIcon from '@mui/icons-material/AccessTime';
import BookmarkIcon from '@mui/icons-material/Bookmark';
import { IconButton, Toolbar } from '@mui/material'
import FavoriteIcon from '@mui/icons-material/Favorite';


import ShareIcon from '@mui/icons-material/Share';

import KeyboardBackspaceIcon from '@mui/icons-material/KeyboardBackspace';
import MenuItem from '@mui/material/MenuItem';


const Img = styled('img')({
  margin: 'auto',
  display: 'block',
  maxWidth: '100%',
  maxHeight: '100%',
});
const buttonSX = {
  "&:hover": {
    borderColor: "#4615b2",
  },
};

const Sidebar = () => {
  return (

    <Grid container >

      <Grid   item xs={3}>


        <div sx={buttonSX} style={{ display: "flex", flexDirection: "column" }}>
          <MenuItem sx={{ mt: 2, mb: 2 }}> <ListItemIcon>
            <CollectionsIcon style={{ color: '#3d3d32' }} ></CollectionsIcon>
          </ListItemIcon>All Photos</MenuItem>
          <MenuItem sx={{ mb: 2 }}>   <ListItemIcon>
            <GroupIcon style={{ color: '#3d3d32' }}></GroupIcon>
          </ListItemIcon>Shared with me</MenuItem>

          <MenuItem sx={{ mb: 2 }}> <ListItemIcon>
            <StarIcon style={{ color: '#3d3d32' }}></StarIcon>
          </ListItemIcon>
            Starred</MenuItem>
          <MenuItem sx={{ mb: 2 }}> <ListItemIcon>
            <AccessTimeIcon style={{ color: '#3d3d32' }}></AccessTimeIcon>
          </ListItemIcon>Recent</MenuItem>

          <div sx={{ mb: 2 }} ><MenuItem sx={{ p: 2 }}>Collections</MenuItem></div>
          <MenuItem sx={{ mt: 2, mb: 2 }}> <ListItemIcon>
            <BookmarkIcon style={{ color: '#3d3d32' }}></BookmarkIcon>
          </ListItemIcon> Photoshoots</MenuItem>
          <MenuItem sx={{ mb: 2 }}> <ListItemIcon>
            <BookmarkIcon style={{ color: '#3d3d32' }}></BookmarkIcon>
          </ListItemIcon> Baseball</MenuItem>
          <MenuItem sx={{ mb: 2 }}> <ListItemIcon>
            <BookmarkIcon style={{ color: '#3d3d32' }}></BookmarkIcon>
          </ListItemIcon> Jazz events</MenuItem>
          <MenuItem sx={{ mb: 2 }}> <ListItemIcon>
            <BookmarkIcon style={{ color: '#3d3d32' }}></BookmarkIcon>
          </ListItemIcon> New Year's Eve 2018</MenuItem>

        </div>
      </Grid>

      <Grid item xs={9}>

        <div>
          <Toolbar variant="dense">
            <IconButton edge="start" color="inherit" aria-label="menu" sx={{ mr: 2 }}>
              < KeyboardBackspaceIcon />
            </IconButton>

            <Grid container alignContent="flex-end">

            </Grid>
            <IconButton size="large" aria-label="search" color="inherit"  >
              <ShareIcon />
            </IconButton>
            <IconButton
              size="large"
              aria-label="display more actions"
              edge="end"
              color="inherit">
              <FavoriteIcon />
            </IconButton>

          </Toolbar>
        </div>
        <div>
          <List sx={{ width: '100%', bgcolor: 'background.paper' }}>
            <ListItem alignItems="flex-start">

              <Grid item>
                <ButtonBase sx={{ width: 120, height: 80, mr:2 }}>
                  
        
                 
                  <Img src="https://hips.hearstapps.com/hmg-prod/images/health-benefits-of-kiwis-1597769097.jpg?crop=1xw:0.75xh;center,top&resize=1200:*" alt="Logo" />
                </ButtonBase>
              </Grid>

              <ListItemText
                primary="kiwis"
                secondary={


                  <Typography
                    sx={{}}

                    color="text.primary"
                  >
                    Collection of high resolution fruit photoshoots for upcoming smoothie recipe cookbook in August.
                  </Typography>


                }

              />
              <Grid container item xs={1} alignContent="flex-end">

              </Grid>
              <h5>
                May 8
              </h5>


            </ListItem>
            <ListItem alignItems="flex-start">

              <Grid item>
                <ButtonBase sx={{ width: 120, height: 80 , mr:2 }}>
                  <Img alt="complex" src="https://www.tastingtable.com/img/gallery/31-types-of-lemons-and-what-makes-them-unique/intro-1656086555.jpg" />
                </ButtonBase>
              </Grid>

              <ListItemText
                primary="Lemons"
                secondary={


                  <Typography
                    sx={{}}

                    color="text.primary"
                  >
                    Collection of high resolution fruit photoshoots for upcoming smoothie recipe cookbook in August. Find edits of lemons in this folder.
                  </Typography>


                }

              />
              <Grid container item xs={1} alignContent="flex-end">

              </Grid>
              <h5>
                May 8
              </h5>


            </ListItem>
            <ListItem alignItems="flex-start">

              <Grid item>
                <ButtonBase sx={{ width: 120, height: 80, mr:2}}>
                  <Img alt="complex" src="http://clv.h-cdn.co/assets/15/22/2560x1728/gallery-1432664914-strawberry-facts1.jpg" />
                
                </ButtonBase>
              </Grid>

              <ListItemText
                primary="Strawberries"
                secondary={


                  <Typography
                    sx={{}}

                    color="text.primary"
                  >
                    Collection of high resolution fruit photoshoots for upcoming smoothie recipe cookbook in August.
                  </Typography>


                }

              />
              <Grid container item xs={1} alignContent="flex-end">

              </Grid>
              <h5>
                May 8
              </h5>


            </ListItem>
            <ListItem alignItems="flex-start">

              <Grid item>
                <ButtonBase sx={{ width: 120, height: 80 ,mr:2 }}>
                  <Img alt="complex" src="https://www.healthifyme.com/blog/wp-content/uploads/2021/12/Benefits-Uses-and-Everything-You-Want-to-Know-About-Fig-Fruit.jpeg" />
                </ButtonBase>
              </Grid>

              <ListItemText
                primary="Figs"
                secondary={


                  <Typography
                    sx={{}}

                    color="text.primary"
                  >
                   Second photoshoots of figs moved here. Use this folder for final edits.
                  </Typography>


                }

              />
              <Grid container item xs={1} alignContent="flex-end">

              </Grid>
              <h5>
                May 8
              </h5>


            </ListItem>
            <ListItem alignItems="flex-start">

              <Grid item>
                <ButtonBase sx={{ width: 120, height: 90 , mr:2 }}>
                  <Img alt="complex" src="https://images.immediate.co.uk/production/volatile/sites/30/2020/08/nectarines-close-up-700-350-d378462.jpg" />
                </ButtonBase>
              </Grid>

              <ListItemText
                primary="Nectarines"
                secondary={


                  <Typography
                    sx={{}}

                    color="text.primary"
                  >
                    Collection of high resolution fruit photoshoots for upcoming smoothie recipe cookbook in August. Find edits of lemons in this folder.
                  </Typography>


                }

              />
              <Grid container item xs={1} alignContent="flex-end">

              </Grid>
              <h5>
                May 8
              </h5>


            </ListItem>
            <ListItem alignItems="flex-start">

              <Grid item>
                <ButtonBase sx={{ width: 120, height: 80 , mr:2 }}>
                  <Img alt="complex" src="https://www.simplyrecipes.com/thmb/_2HrCRuI2BwfVFJFOCS3-dF0gME=/1500x0/filters:no_upscale():max_bytes(150000):strip_icc()/Simply-Recipes-Guide-to-Watermelon-BODY-02-53c00ad4bdb641c995f62be94a833e22.jpg" />
                </ButtonBase>
              </Grid>

              <ListItemText
                primary="Watermelons"
                secondary={


                  <Typography
                    sx={{}}

                    color="text.primary"
                  >
                    Collection of high resolution fruit photoshoots for upcoming smoothie recipe cookbook in August.
                  </Typography>


                }

              />
              <Grid container item xs={1} alignContent="flex-end">

              </Grid>
              <h5>
                May 8
              </h5>


            </ListItem>
          </List>
        </div>
      </Grid>
    </Grid>




  )
}

export default Sidebar
